/*
Snake game using HTML5 canvas and javascript

File-name : snake.js
Description: snake game back-end work
Files: index.html and snake.js

Author: Karan Kanwal
Contact: karan.kanwal@mountblue.io
*/


var snake = document.querySelector('canvas');

snake.width = 600;
snake.height = 600;

var s = snake.getContext('2d');

var box = 30;
var score =0;

//food
let food = {
    x : ((Math.floor(Math.random()*19))+0.5) *box,
    y : ((Math.floor(Math.random()*19))+0.5) *box
}
 
//snake
let snakeI = [];
    snakeI[0] = {
        x:9*box/2,
        y:9*box/2
}

//control the snake
let d;
let command = "play";

document.addEventListener("keydown", direction);

//pause-play command
let paused = false;

function pause(){
    if(paused){
        paused = false;
    }else{
        paused = true;
    }
}



function direction(event){
    
    if(event.keyCode==37 && d!="Right"){
        d="Left";
    }else if(event.keyCode==38 && d!="Down"){
        d="Up";
    }else if(event.keyCode==39 && d!= "Left"){
        d="Right";
    }else if(event.keyCode==40 && d!="Up"){
        d="Down";
    }

    if(event.keyCode ==  32 ){
        pause();
     }
}
//collision
function collision(head,array){
    for(let i=0; i<array.length; i++){
        if(head.x == array[i].x && head.y==array[i].y){
            return true;
        }
    }
    return false;
}

var colorB = "black";
document.getElementById('blue').addEventListener('click', function(){
    colorB = "blue";
    draw();
});
document.getElementById('red').addEventListener('click', function(){
    colorB = "red";
    draw();
});
document.getElementById('black').addEventListener('click', function(){
    colorB = "black";
    draw();
});
document.getElementById('yellow').addEventListener('click', function(){
    colorB = "yellow";
    draw();
});

function draw(){
    if(paused){

    }else{
        document.getElementById('score').innerHTML = score;
    s.fillStyle=colorB;
    s.fillRect(0,0,snake.width,snake.height);

    //snake
    for (let i=0; i<snakeI.length; i++){
        s.beginPath();
        s.strokeStyle = (i==0)?"green":"white";
        s.arc(snakeI[i].x, snakeI[i].y, box/2,0,Math.PI*2,false);
        s.stroke();
        s.fillStyle = (i==0)?"green":"black";
        s.fill();
    }

    //food
    s.beginPath();
    s.strokeStyle = "white";
    s.arc(food.x, food.y, box/2,0,Math.PI*2,false);
    s.stroke();
    s.fillStyle = "white";
    s.fill();

    
    //old-head position
    let snakeX = snakeI[0].x;
    let snakeY = snakeI[0].y;
    
    //movement of snake
    if (d=="Left") snakeX-=box;
    if (d=="Up")   snakeY-=box;
    if (d=="Right")snakeX+=box;
    if (d=="Down") snakeY+=box;
    



    //if snake eats the food
    if (snakeX == food.x && snakeY == food.y){
        score++;
            food={
                x : ((Math.floor(Math.random()*19))+0.5) *box,
                y : ((Math.floor(Math.random()*19))+0.5) *box
        }
    }else{
        //remove the tail
        snakeI.pop();    
    }

    //newHead
    let newHead = {
        x: snakeX,
        y: snakeY
    }   

    //game-over
    if(snakeX< 0 || snakeX>20*box || snakeY<0 || snakeY>20*box || collision(newHead,snakeI)){
        s.font = "30px Arial";
        s.fillText("Game over", snake.width/2-50, snake.height/2-25);
        clearInterval(game);
    }

    snakeI.unshift(newHead);

    }
    
   
}
let time;
let game = draw();  
//snake-steps at a time
document.getElementById('low').addEventListener('click', function (){
    game =setInterval(draw, 150);
});
document.getElementById('medium').addEventListener('click', function (){
    game =setInterval(draw, 100);
});
document.getElementById('high').addEventListener('click', function (){   
    game =setInterval(draw, 50);
});



//restart the game
document.getElementById('restart').addEventListener('click', function(){
    location.reload();
});







